<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\Student_Data\Student_Data;
use App\Bitm\SEIP137157\Student_Data\Utility;
$student_Data= new Student_Data();
$student_Data->prepare($_REQUEST);
$singleItem=$student_Data->view();
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit - Student_Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
</head>
<body background="../../../resource/images/bgimage.png">







<div class="container">



    <h2>Select Student Data</h2>
    <br>
    <br>
    <form role="form" method="get" action="update.php">

        <input type="hidden" name="id" value="<?php echo $singleItem->id?>">

        <label>Please Enter Full Name </label>
        <input type="text" name="full_name" value="<?php echo $singleItem->full_name?>" >

        <br>
        <br>

        <label>Please Select Course Names </label>
        <div class="checkbox">
            <label><input type="checkbox"  <?php if(stripos($singleItem->course_name,"PHP")!==false) echo "checked";?> name=course_name[] value="PHP">PHP</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" <?php if(stripos($singleItem->course_name,"JAVA")!==false) echo "checked";?> name=course_name[] value="JAVA">JAVA</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" <?php if(stripos($singleItem->course_name,"PYTHON")!==false) echo "checked";?> name=course_name[] value="PYTHON" >PYTHON</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" <?php if(stripos($singleItem->course_name,"DOTNET")!==false) echo "checked";?> name=course_name[] value="DOTNET">DOTNET</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" <?php if(stripos($singleItem->course_name,"ORACLE")!==false) echo "checked";?> name=course_name[] value="ORACLE">ORACLE</label>
        </div>

        <div class="checkbox">
            <label><input type="checkbox" <?php if(stripos($singleItem->course_name,"HTML")!==false) echo "checked";?> name=course_name[] value="HTML">HTML</label>
        </div>

        <input type="submit" value="Submit">
    </form>
</div>






</body>
</html>
