<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\Student_Data\Student_Data;
use App\Bitm\SEIP137157\Student_Data\Utility;

$student_Data= new Student_Data();
$student_Data->prepare($_REQUEST);
$singleItem=$student_Data->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View a Student Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body >

<div class="container">
    <h2>Student Information of <?php echo $singleItem->full_name?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">Full Name: <?php echo $singleItem->full_name?></li>
        <li class="list-group-item">Course Name: <?php echo $singleItem->course_name?></li>

    </ul>
</div>

</body>
</html

