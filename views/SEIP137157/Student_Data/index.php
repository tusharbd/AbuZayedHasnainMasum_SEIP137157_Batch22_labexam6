<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\Student_Data\Student_Data;
use App\Bitm\SEIP137157\Student_Data\Utility;
use App\Bitm\SEIP137157\Student_Data\Message;

$student_Data = new Student_Data();

$recordSet =  $student_Data->index();

?>

<!DOCTYPE html>

<head>
    <title>List Student Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
</head>
<body background="../../../resource/images/bgimage.png">
<div class="container">
    <h2>List Student Data</h2>


        
    <table>
        <tr>
            <td height="100">
                <div id="TopMenuBar">
                    <button type="button" onclick="window.location.href='create.php'" class=" btn-primary btn-lg">Add new</button>
                    <button type="button" onclick="window.location.href='trashed.php'" class=" btn-success btn-lg">View Trashed Items</button>
                </div>
            </td>

            <td width = "50">

            </td>

            <td height="100" >

                <div id="message" >

                    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                        echo "&nbsp;".Message::message();
                    }
                    Message::message(NULL);

                    ?>
                </div>

            </td>
        </tr>
    </table>




    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>               

                <th>Serial</th>
                <th>ID</th>
                <th>Full Name</th>
                <th>Course Name</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>

            <?php
             $serial =0;

             while($row = $recordSet->fetch_assoc() ) {


           ?>

                <tr <?php if($serial%2) echo 'bgcolor="#fffaf0"'; else echo 'bgcolor="#f8f8ff"';?>>


                    <td>
                        <?php echo ++$serial?>
                    </td>

                    <td >
                        <?php echo $row["id"]?>
                    </td>

                    <td>
                        <?php echo  $row["full_name"]?>
                    </td>

                    <td>
                        <?php echo  $row["course_name"]?>
                    </td>

                    <td >

                        <a href="view.php?id=<?php echo $row["id"]?>" class="btn btn-primary" role="button">View</a>
                        <a href="edit.php?id=<?php echo $row["id"]?>"  class="btn btn-info" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $row["id"]?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete(<?php echo $row["id"]?>)">Delete</a>
                        <a href="trash.php?id=<?php echo $row["id"]?>"  class="btn btn-info" role="button">Trash</a>

                    </td>
                </tr>
                <?php
            }// end of while
            ?>
            </tbody>
        </table>

    </div>
</div>



</body>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>




<script>
    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete ID# "+id+" ?");
        if (x)
            return true;
        else
            return false;
    }




    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });





</script>


</HTML>

