<!DOCTYPE html>

<head>
    <title>Add Student_Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
</head>

<body >



<div class="container">



    <h2>Select Student Data</h2>
    <br>
    <br>
    <form role="form" method="get" action="store.php">
        <label>Please Enter Full Name </label>
        <input type="text" name="full_name" placeholder="Your Full Name Here" >

        <br>
        <br>

        <label>Please Select Course Names </label>
        <div class="checkbox">
        <label><input type="checkbox" name=course_name[] value="PHP">PHP</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=course_name[] value="JAVA">JAVA</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=course_name[] value="PYTHON" >PYTHON</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=course_name[] value="DOTNET">DOTNET</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=course_name[] value="ORACLE">ORACLE</label>
        </div>

        <div class="checkbox">
            <label><input type="checkbox" name=course_name[] value="HTML">HTML</label>
        </div>

        <input type="submit" value="Submit">
    </form>
</div>


</html>
