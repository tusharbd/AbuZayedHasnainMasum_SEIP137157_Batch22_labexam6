<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\Student_Data\Student_Data;

use App\Bitm\SEIP137157\Student_Data\Utility;

$selected_course_name= $_REQUEST['course_name'];
$comm_separated=  implode(",",$selected_course_name);

$_REQUEST['course_name']=$comm_separated;

$student_Data = new Student_Data;

$student_Data->prepare($_REQUEST);
$student_Data->store();

