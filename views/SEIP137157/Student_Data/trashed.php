<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\Student_Data\Student_Data;
use App\Bitm\SEIP137157\Student_Data\Utility;
use App\Bitm\SEIP137157\Student_Data\Message;


$student_Data = new Student_Data();
$recordSet =  $student_Data->trashed();


?>

<!DOCTYPE html>

<head>
    <title>Trashed List - Student Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
</head>
<body background="../../../resource/images/bgimage.png">



<div class="container">
    <h2>Trashed List - Student Data</h2>
    <form action="recovermultiple.php" method="get" id="multiple">
        <table>
            <tr>
                <td height="100">
                    <div id="TopMenuBar">
                        <button type="button" onclick="window.location.href='index.php'" class="btn-lg btn-primary">Student Data List</button>
                        <button type="submit"  class="btn-lg  btn-success">Recover Selected</button>
                        <button type="button"  class="btn-lg btn-danger" id="multiple_delete">Delete Selected</button>

                    </div>
                </td>

                <td width = "50">

                </td>

                <td height="100" >

                    <div id="message" >

                        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                            echo "&nbsp;".Message::message();
                        }
                        Message::message(NULL);

                        ?>
                    </div>

                </td>
            </tr>
        </table>





        <div class="table-responsive">

            <table class="table">
                <thead>
                <tr>
                    <th width = "150">  Check Item(s)</th>
                    <th width ="35" align="left">Serial</th>
                    <th width="35"  align="left">ID</th>
                    <th width="200" align="left">Student_Data</th>
                    <th align="left">Action</th>
                </tr>
                </thead>

                <tbody>



                <?php
                $serial = 0;


                while($row = $recordSet->fetch_assoc() ) {



                    ?>



                    <tr <?php if($serial%2) echo 'bgcolor="#fffaf0"'; else echo 'bgcolor="#f8f8ff"';?>>
                        <td width = "100" align="center">
                            <input type="checkbox" name="mark[]" value="<?php echo  $row["id"]?>">
                        </td>


                        <td  width = "100" align="left">
                            <?php echo ++$serial?>
                        </td>

                        <td width = "100" align="left">
                            <?php echo $row["id"]?>
                        </td>

                        <td width="400" align="left">
                            <?php echo  $row["course_name"]?>
                        </td>

                        <td align="left">
                            <a href="recover.php?id=<?php echo $row["id"]?>" class="btn btn-success" role="button">Recover</a>
                            <a href="delete.php?id=<?php echo $row["id"]?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete(<?php echo $row["id"]?>)">Delete</a>
                        </td>
                    </tr>
                    <?php
                }// end of while
                ?>
                </tbody>
            </table>
    </form>
</div>
</div>

</body>

<script>

    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();


    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete ID# "+id+" ?");
        if (x)
            return true;
        else
            return false;
    }




    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    $(document).ready(function() {
        $("#checkall").click(function() {
            var checkBoxes = $("input[name=mark\\[\\]]");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
    });




</script>


</HTML>
