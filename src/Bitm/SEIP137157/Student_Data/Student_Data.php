<?php
namespace App\Bitm\SEIP137157\Student_Data;
use App\Bitm\SEIP137157\Student_Data\Utility;
use App\Bitm\SEIP137157\Student_Data\Message;

class Student_Data{
    public $id="";
    public $full_name="";
    public $course_name="";
    public $conn;
    public $deleted_at;
    public $refferarFilename;

    public function prepare($data=""){

        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        if(array_key_exists("full_name",$data)){
            $this->full_name=$data['full_name'];
        }

        if(array_key_exists("course_name",$data)){
            $this->course_name=$data['course_name'];

        }

    }



    public  function __construct($student_data=false)
    {
        if (isset($_SERVER['HTTP_REFERER']) ) {
            $this->refferarFilename = basename((string)$_SERVER['HTTP_REFERER']);
            if (strpos($this->refferarFilename, ".php") == false) $this->refferarFilename = "index.php";
        } else $this->refferarFilename = "index.php";

        $this->conn=mysqli_connect("localhost","root","","labxm6b22") or die("Database connection failed");
    }

    

    
    public function store(){
        $query="INSERT INTO `labxm6b22`.`student_data` (`full_name`,`course_name`) VALUES ('".$this->full_name."','".$this->course_name."')";
      //  Utility::dd($query);

        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect("index.php");
        }
        else {
            echo "Error";
        }
    }


    //start of common methods

    public function index(){
        $query = "SELECT * FROM `student_data` WHERE `deleted_at` IS  NULL";
        $recordSet = mysqli_query($this->conn,$query);
        return $recordSet;
    }// end of index()


    public function view(){
        $query="SELECT * FROM `student_data` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;


    }// end of view()




    public function update(){
        $query="UPDATE `labxm6b22`.`student_data` SET  `full_name` = '".$this->full_name."', `course_name` = '".$this->course_name."' WHERE `student_data`.`id` = ".$this->id;
       // Utility::dd($query);

        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        }
        else {
            echo "Error";
        }
    }


    public function delete(){
        $query= "DELETE FROM `student_data` WHERE `student_data`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
               <div class=\"alert alert-success\">
                    <strong>Success!</strong> Data has been deleted successfully.
                </div>");
        }
        else {
            echo "Error";
        }

        Utility::redirect($this->refferarFilename);
    }// end of delete()


    //end of common methods





    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `labxm6b22`.`student_data` SET `deleted_at` =" . $this->deleted_at . " WHERE `student_data`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                     <div class=\"alert alert-info\">
                            <strong>Deleted!</strong> Data has been trashed successfully.
                     </div>");
            Utility::redirect($this->refferarFilename);
        } else {
            Message::message("
                     <div class=\"alert alert-info\">
                             <strong>Deleted!</strong> Data has not been trashed successfully.
                     </div>");
            Utility::redirect($this->refferarFilename);
        }
    }

    public function trashed()
    {
        $query = 'SELECT * FROM `student_data` WHERE `deleted_at` IS NOT NULL';
        $recordSet = mysqli_query($this->conn, $query);
        return $recordSet;
    }

    public function recover()
    {

        $query = "UPDATE `labxm6b22`.`student_data` SET `deleted_at` = NULL WHERE `student_data`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
              <div class=\"alert alert-info\">
                 <strong>Deleted!</strong> Data has been recovered successfully.
              </div>");
            Utility::redirect($this->refferarFilename);
        } else {
            Message::message("
              <div class=\"alert alert-info\">
                 <strong>Deleted!</strong> Data has not been recovered successfully.
              </div>");
            Utility::redirect($this->refferarFilename);
        }

    }

    public function recoverSelected($IDs = Array())
    {

        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `labxm6b22`.`student_data` SET `deleted_at` = NULL WHERE `student_data`.`id` IN(" . $ids . ")";

            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                  <div class=\"alert alert-success\">
                        <strong>Recovered!</strong> Selected Data has been recovered successfully.
                  </div>");
                Utility::redirect($this->refferarFilename);
            } else {
                Message::message("
                  <div class=\"alert alert-info\">
                       <strong>Sorry!</strong> Selected Data has not been recovered successfully.
                 </div>");
                Utility::redirect($this->refferarFilename);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($this->refferarFilename);
        }
    }















    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `labxm6b22`.`student_data` WHERE `student_data`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                 <div class=\"alert alert-info\">
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                 </div>");
                Utility::redirect($this->refferarFilename);
            } else {
                Message::message("
               <div class=\"alert alert-info\">
                   <strong>Deleted!</strong> Selected Data has not been deleted successfully.
               </div>");
                Utility::redirect($this->refferarFilename);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($this->refferarFilename);
        }

    }






    public function __destruct()
    {
        //echo "<b>Leaving Student_Data Class<b>";
        //echo "<hr>";
    }




}
